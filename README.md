Some changes to files from server payara 4.

Main change is integration of modified version of SPNEGO (changed from filter to callable lib - public in our other repository) into standart FormAuthneticator. Which means, that user will automatically login on computer, which is in domain and if it isn't, then there will be shown standart login form.

This solution currently depends on custom realm, which authenticate user (pre authenticated by SPNEGO) with special trusted key (password). (This realm is currently not public, but writing own realm is not that much of problem ... https://javaee.github.io/glassfish/doc/4.0/application-development-guide.pdf -> "Creating a Custom Realm")
